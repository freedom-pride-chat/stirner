import Command from './Command';
import Context from "./Context";

export default class CommandStart implements Command {
    exec(context: Context): boolean {
        context.send("Вас приветствует бот частной дискриминации ФПЧ.");
        return true;
    }
}

module.exports = CommandStart;
