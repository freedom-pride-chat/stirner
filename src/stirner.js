// Stirner Bot - Telegram Bot
// Copyright (C) 2022  Yury Moskov
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
//     You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import TelegramBot from 'node-telegram-bot-api';
import CommandStart from './CommandStart';
import ArrowCommand from './ArrowCommand';
import Context from './Context';

const fpc = -1001798667684;

require('dotenv').config();
const token = process.env.TELEGRAM_BOT_TOKEN;
const bot = new TelegramBot(token, {polling: true});

let period = 1000*60*60*12;
let timestamp = Date.now();
let nextTimestamp = Math.ceil(Date.now() / period) * period;
let offset = nextTimestamp - timestamp;

const every12h = (target = fpc) => {
    bot.sendMessage(target, "Франциск ничего не имел против того, чтобы дать выкуп за свое освобождение, но цена, назначенная Карлом, казалась ему слишком высокой и несправедливой.");
};

setTimeout(() => {
        every12h();
        setInterval(() => {
            every12h();
        }, period)
    }, offset
);

const ListCommand = new ArrowCommand((context) => {
    const friendly_chats = ["@freedompride_chat", "@panarchy"];
    let chat_messages = "";
        friendly_chats.forEach(function(item) {
        chat_messages += "\n" + item;
    });
    context.send('Список дружественных чатов:' + chat_messages);
    return true;
});

const HelpCommand = new ArrowCommand((context) => {
    context.send("/start - приветствие бота частной дискриминации\
                \n/list = /chatlist - список дружественных чатов\
                \n/quote - цитата Единственного\
                \n/try [текст] - орёл и решка мира ФПЧ\
                \n/source - ссылка на исходный код\
                \n/random [максимальное число] - вывод случайного числа, не достигающего максимального числа\
                \n/help - вывод этого сообщения");
    return true;
});

let quotes = ["Кроме меня для меня ничего нет", "Скалу, преграждающую мне путь, я обхожу до тех пор, пока у меня не наберется достаточно пороха, чтобы её взорвать", "Человеку же «истинно хорошо» только тогда, когда он также и «духовно свободен». Человек – дух, и поэтому все силы враждебные ему, духу, все сверхчеловеческие, небесные, нече­ловеческие силы должны быть разрушены и выше всего должен быть поставлен Человек.", "Меня хотят уговорить пожертвовать своими интересами ради интересов государства. Я, напротив того, объявляю войну не на жизнь, а на смерть всякому государству, даже самому демократическому.", "И разве жертвующие собой не своекорыстны, не эгоисты? Имея только одну страсть, они заботятся только о ее удовлетворении, но зато с тем большим усердием: они всецело уходят в нее.", "...не утруждай себя преградами других; достаточно, если ты разрушишь свои собственные.", "Ничто — вот на чём я построил своё дело.", "Когда господствует разум, гибнет личность.", "Свобода Человека, согласно принципам политического либерализма,— это свобода от личностей, это личная свобода. Никто не может ничего приказать: повелевает один лишь закон.", "Буржуазия — это заслуженное дворянство."];
quotes.sample = function() {
    return this[Math.floor(Math.random()*this.length)];
};

const QuoteCommand = new ArrowCommand((context) => {
    context.send(quotes.sample());
    return true;
});

const commands = {
    "/start": new CommandStart(),
    "/chatlist": ListCommand,
    "/list": ListCommand,
    "/quote": QuoteCommand,
    "/check": new ArrowCommand((context) => {
        context.send("Никто не может ничего мне приказать.");
        return true;
    }),
    "/source": new ArrowCommand((context) => {
        /* You must provide the source code to any user who interact with modified version of bot. Such users have the right to receive a copy of the source code. */
        context.send("Исходный код Штирнера:\n\nhttps://gitlab.com/freedom-pride-chat/stirner");
        return true;
    }),
    "/help": HelpCommand
};

bot.onText(/\/try(?:@StirnerBot)? (.+)/, (msg, match) => {
    let result = (Math.random() >= 0.5) ? " [УДАЧНО]" : " [ПРОВАЛ]";
    const response = match[1];
    const context = new Context(bot, msg);

    context.send(response + result).catch((error) => {
        context.send("Но если он и сознает, что его сообщение в высшей степени ценно для других, то он все же искал свою истину не для других, а для себя, ради себя, ибо в нем была эта потребность, ибо неизвестность не давала ему покоя, пока он, насколько это было в его силах, не рассеял тьму и не внес свет и ясность.")
    });
});

bot.onText(/\/rand(?:om)?(?:@StirnerBot)? (\d+)/, (msg, match) => {
    let maximum = Number(match[1]);
    if (maximum > Number.MAX_VALUE) {
        if (Math.random() > 0.5) maximum = Number.MAX_VALUE;
    }

    const context = new Context(bot, msg);
    const num = Math.ceil(maximum*Math.random());
    context.send(String(num));
    if (Math.random() > 0.95) context.send("Не будем более рабами случая! Создадим новый порядок, который покончит с колебаниями на земле. И такой порядок да будет свят!");
});

bot.on('message', (msg) => {
    const context = new Context(bot, msg);
    if (msg.text[0] === "/")
    {
        const parameters = msg.text.split(' ');
        const string = parameters[0].split('@')[0];
        let command = commands[string];
        if (command) {
            if (command.exec(context)) {
                return;
            }
        }
    }

    if (msg.chat.id === 510999358)
    {
        context.send(JSON.stringify(msg));
    }
});
