import Context from "./Context";

export default interface Command {
    exec: (context: Context) => boolean;
}
