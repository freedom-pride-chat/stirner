import TelegramBot from 'node-telegram-bot-api';

// TODO: type for msg

export default class Context {
    protected bot: TelegramBot;
    protected msg: any;

    constructor(bot: TelegramBot, msg: any) {
       this.bot = bot;
       this.msg = msg;
    }

    send(message: string) {
        // console.log(this.msg.chat.id + " => " + message);
        return this.bot.sendMessage(this.msg.chat.id, message);
    }

    sendWithOptions(message: string, options: any) {
        // console.log(this.msg.chat.id + " => " + message);
        return this.bot.sendMessage(this.msg.chat.id, message, options);
    }
}

module.exports = Context;
