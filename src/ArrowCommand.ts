import TelegramBot from 'node-telegram-bot-api';
import Command from './Command';
import Context from "./Context";

export default class ArrowCommand implements Command {
    protected exec_: (context: Context) => boolean;
    constructor(exec: (context: Context) => boolean) {
        this.exec_ = exec;
    }

    exec(context: Context): boolean {
        return this.exec_(context);
    }
}

module.exports = ArrowCommand;
