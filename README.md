## About Max Stirner

**Johann Kaspar Schmidt** (25 October 1806 – 26 June 1856), known professionally as **Max Stirner**, was a German post-Hegelian philosopher, dealing mainly with the Hegelian notion of social alienation and self-consciousness. Stirner is often seen as one of the forerunners of nihilism, existentialism, psychoanalytic theory, postmodernism and individualist anarchism.

Stirner's main work, *The Unique and Its Property* (German: *Der Einzige und sein Eigentum*), was first published in 1844 in Leipzig and has since appeared in numerous editions and translations.

Source: https://en.wikipedia.org/wiki/Max_Stirner

## Install
```
sudo pacman -S nodejs git typescript --needed
git clone https://gitlab.com/freedom-pride-chat/stirner.git
cd stirner && yarn && tsc
```

## Set up
```
echo "TELEGRAM_BOT_TOKEN=$YOUR_TELEGRAM_BOT_TOKEN" > .env
```

## Run
```
node built/stirner.js
```

## License
```
    Stirner Bot - Telegram Bot
Copyright (C) 2022  Yury Moskov

    This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
